function swap() {
  document.getElementById('#swap').onclick(function () {
    let v1 = document.getElementById('#origin-currency').value;
    let v2 = document.getElementById('#converted-currency').value;

    document.getElementById('#origin-currency').value = document.getElementById('#converted-currency').value;
    document.getElementById('#converted-currency').value = document.getElementById('#origin-currency').value;
  });
}

$('#swap').click(function () {
  var v1 = $('#s1').val(),
    v2 = $('#s2').val();
  $('#s1').val(v2);
  $('#s2').val(v1);
});
