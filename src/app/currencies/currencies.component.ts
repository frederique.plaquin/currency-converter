import { Component, OnInit } from '@angular/core';

import { Currency } from "../models/currency";
import { CURRENCIES } from "../mock/currencies";

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.css']
})
export class CurrenciesComponent implements OnInit {

  require: any;
  public apiCurrencies!: Currency[];
  public currencies!: Currency[];

  constructor() {

  }

  ngOnInit(): void {
    this.currencies = CURRENCIES
    fetch('https://freecurrencyapi.net/api/v2/latest?apikey=04ff5f20-8402-11ec-b6d6-a1ee1995e8b8')
      .then((response: any) => {
        console.log(response)
        this.apiCurrencies = response;
      })
      .then((data:any) => {
        console.log(data)
        this.currencies.push(data);
      })
      .catch((error: any) => {
        console.log(error)
      })
  }

}
