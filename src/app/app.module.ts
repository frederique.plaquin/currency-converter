import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { ConvertFormComponent } from './convert-form/convert-form.component';
import { CurrenciesComponent } from './currencies/currencies.component';
import { CurrencyComponent } from './currency/currency.component';
import { HttpClientModule } from "@angular/common/http";
import { Currency } from "./models/currency";


@NgModule({
  declarations: [
    AppComponent,
    ConvertFormComponent,
    CurrenciesComponent,
    CurrencyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [Currency],
  bootstrap: [AppComponent]
})
export class AppModule { }
