import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Currency } from "../models/currency";
import { CurrenciesService } from "../services/currencies.service";
import { CURRENCIES } from "../mock/currencies";
import { Subscription } from "rxjs";
import {stringify} from "@angular/compiler/src/util";


@Component({
  selector: 'app-convert-form',
  templateUrl: './convert-form.component.html',
  styleUrls: ['./convert-form.component.css']
})
export class ConvertFormComponent implements OnInit {

  public currencies!: Currency[];

  public apiResponse: any;
  apiResponseData: any;
  convertForm!: FormGroup;
  startSwap:any;
  endSwap: any;
  temp:any;
  devise!: Currency;
  deviseSubscription!: Subscription;
  options!:any;

  constructor(private formBuilder: FormBuilder,
              private currenciesService: CurrenciesService,
              private currency: Currency) { }

  ngOnInit() {
    this.currencies = CURRENCIES
    this.devise = this.currency

    /* this.currenciesService.getCurrencies().subscribe(
      (currencies: any) => {
        this.apiResponse = currencies['data'];
        console.log('form in form: ' + this.apiResponse)
        console.log(currencies['data'])

        this.currenciesService.emitCurrencies()



        Object.entries(this.apiResponse).forEach(
          ([key, value]) => {
            console.log(key, value)
            this.devise.name = key;
            this.devise.change = value;
            console.log(this.devise)

            this.currencies.push(this.devise)

          }
        )
      }
    ) */



    this.currenciesService.getCurrenciesList()
    this.currenciesService.emitCurrencies()


    this.initForm()
  }

  initForm() {
    this.convertForm = this.formBuilder.group({
      amount: [''],
      crControl: [''], // pattern prend comme arg une expression regulière (regex) ici obligerl tuilisateur a entrer au moins 6 caractères de type alphanumérique comme demandé par firebase
      crEndControl: ['']
    });
    this.startSwap = this.convertForm.get('crControl');
    this.endSwap = this.convertForm.get('crEndControl');

    this.currenciesService.currencySubject.subscribe((currencies: object) => {
      let deviseKeys = Object.entries(currencies).entries()
      this.options = this.currenciesService.options


      this.currencies.push(this.devise)
      console.log(this.options)

      Object.entries(currencies).forEach(
        ([key, value]) => {
          console.log(key, value)
          this.devise.name = key;
          this.devise.change = value;
          console.log(this.devise)

          this.currencies.push(this.devise)

        }
      )
      console.log('converted amount: ' + this.currenciesService.getConversion(10, 'EUR', 'JPY'))

      console.log('form in form: ' + this.apiResponse)
      console.log(currencies)
    })




    console.log('form: ' + this.currencies)
    // this.apiResponse = this.currenciesService.getCurrencies()
    // console.log(this.apiResponse)
  }

  onClick() {
    console.log(this.startSwap.value + ' - ' + this.endSwap.value);
    this.convertForm = this.formBuilder.group({
      crControl: this.endSwap,
      crEndControl: this.startSwap
    })
    this.temp = this.startSwap;
    this.startSwap = this.endSwap;
    this.endSwap = this.temp;
  }

  getList() {
    this.apiResponse.forEach((devise: any) => console.log(devise))
  }

}
