import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { Currency } from "../models/currency";
import { CURRENCIES } from "../mock/currencies";



@Injectable({
  providedIn: 'root'
})
export class CurrenciesService {

  currencies: Currency[] = [];
  apiResponse: any;
  apiCurrencies: Currency[] = [];
  currency!: Currency;
  currencySubject = new Subject<Currency[]>();
  devise!: Currency;
  options!:any;
  convert!:any;


  constructor(private http: HttpClient) { }

  emitCurrencies() {
    this.currencySubject.next(this.currencies)
  }

  getCurrencies() {
    let url = 'https://freecurrencyapi.net/api/v2/latest?apikey=04ff5f20-8402-11ec-b6d6-a1ee1995e8b8'
    return this.http.get(url)

  }

  getCurrenciesList() {
    let url = 'https://freecurrencyapi.net/api/v2/latest?apikey=04ff5f20-8402-11ec-b6d6-a1ee1995e8b8'
    return this.http.get(url)
      .subscribe({
        next: data => this.apiResponse = data,
        error: err => console.log(err),
        complete: () => {
          this.currencies = this.apiResponse['data']
          this.options = Object.keys(this.apiResponse.data);
          console.log(this.apiResponse)
          this.emitCurrencies()
          console.log(this.currencies)
          console.log(this.options)
        }
      })
  }

  getConversion(amount: number, from: string, to: string){
    let amountToConvert = amount
    let currencyFrom = from
    let currencyTo = to
    let exchangeRate = this.apiResponse['data'][to]
    let convertedAmount = null

    console.log('rate: ' + exchangeRate)

    convertedAmount = amount * exchangeRate

    return convertedAmount


  }
}
